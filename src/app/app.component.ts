import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
  <form class="k-form">
  <label class="k-form-field">
    <input type="checkbox"
         name="Enforce" id="enforce" class="k-checkbox">
    <label for="enforce" class="k-checkbox-label">Enforce</label>
  </label>
  <label class="k-form-field">
    <input type="radio"
         name="IndentUsing" id="intend-tabs" value="1" class="k-radio"/>
    <label class="k-radio-label" for="intend-tabs">Indent using tabs</label>
  </label>

  <kendo-dropdownlist
  [value]="'Expressions'"
  [data]="
[
'expressions/arithmetic',
'expressions/comparison',
'expressions/logic'
]">
</kendo-dropdownlist>
</form>
  `,
  styles: []
})
export class AppComponent {
  title = 'kendo-ui-playground';
}
